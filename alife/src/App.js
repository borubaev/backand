import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Nav, Navbar, Form, Button, Container} from "react-bootstrap";
import Product from "./Product";
import {createContext, useEffect, useState} from 'react'
import {BrowserRouter, Routes, Route, Link} from "react-router-dom";
import Korzina from "./Korzina";

export const MyContext = createContext({})


function App() {
    const [basket, setBasket] = useState(JSON.parse(localStorage.getItem('key')) || [])
    const [poisk,setPoisk] = useState('')
    const korz = () => {
        setPoisk('korzina')
    }

    const inctotal = (i) => {
        setBasket(q => {
            return q.map((w) => {
                if (w.id === i.id) {
                    w.total += 1
                }
                return w
            })
        })
    }
    const subtotal = (i) => {
        setBasket(q => {
            if (i.total === 1) {
                return q.filter((e) => e.id !== i.id)
            } else {
                return q.map(z => {
                    if (z.id === i.id) {
                        z.total -= 1

                    }
                    return z
                })
            }
        })
    }
    useEffect(() => {
        localStorage.setItem('key', JSON.stringify(basket))
    }, [basket])
    console.log((poisk))
    return (
        <>
            <MyContext.Provider value={{basket, setBasket, subtotal, inctotal}}>
                <BrowserRouter>
                    <Navbar bg="dark" variant='dark' expand="lg"
                            style={{position: 'fixed', top: '0', width: '100%', zIndex: '5'}}>
                        <Container fluid style={{display: 'flex', justifyContent: 'space-bbetween'}}>
                            <Navbar.Brand href="#" style={{display: 'flex', alignItems: 'center'}}>
                                <img style={{margin: '10px'}}
                                     alt=""
                                     src="/logo.jpg"
                                     width="60"
                                     height="60"
                                     className="d-inline-block align-top"
                                />
                                ALife_Market</Navbar.Brand>
                            <Navbar.Collapse id="navbarScroll" style={{flexBasis: '25vw', flexGrow: '0'}}>
                                <Form className="d-flex">
                                    <Nav>
                                        <Nav.Link href="#action1" as={Link} to={`/`}>Главная</Nav.Link>
                                        <Nav.Link href="#action1" as={Link}
                                                  to={`/basket`} onClick={korz}>Корзина({basket.length})</Nav.Link>
                                    </Nav>
                                    <Form.Control
                                        type="search"
                                        placeholder="Search"
                                        className="me-2"
                                        aria-label="Search"
                                    />
                                    <Button variant="outline-success">Search</Button>
                                </Form>
                            </Navbar.Collapse>
                        </Container>
                    </Navbar>
                    <Routes>
                        <Route path={`/`} element={<Product/>}/>
                        <Route path={`/basket`} element={<Korzina/>}/>
                    </Routes>
                </BrowserRouter>
            </MyContext.Provider>
        </>
    )
}

export default App;
