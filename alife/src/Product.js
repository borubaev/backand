import {Button, Card, Col, Row, Container} from "react-bootstrap";
import data from './products'
import {useContext} from "react";
import {MyContext} from "./App";
import numberSeparator from 'number-separator'


function Product() {
    const Context = useContext(MyContext)
    const add = (i) => {
        Context.setBasket(q =>{
            const a = [...q,{total: 1, id: i.id}]
            return a
        })
    }
console.log(Context.basket)

    return (
        <>
            <div className={'products'}>
                <Container style={{marginTop:'7vh'}}>
                    <Row md={9}>
                        {data.map(q => {
                            const serch = Context.basket.find((e) => {
                                return e.id === q.id
                            })
                            return (
                                <Col md={3}>
                                    <Card className={'content'}>
                                        {q.discount ? <div style={{position:'absolute'}} className={'discount d1'}>-{q.discount}%</div>:null}
                                        <div style={{width: '100%', height: '45vh'}}>
                                            <div className={'img'} style={{backgroundImage: `url(${q.image})`}}/>
                                        </div>
                                        <div className={'title a'}>
                                            <div>
                                                <Card.Title>{q.title}</Card.Title>
                                                <Card.Title>{q.good}</Card.Title>
                                                {q.discount ?
                                                    <>
                                                        <Card.Title>
                                                            <h2>{numberSeparator((q.price - (q.price / 100 * q.discount)).toFixed(2))} KGS</h2>
                                                        </Card.Title>
                                                        <Card.Title>
                                                            <s>{q.price} KGS</s>
                                                        </Card.Title>
                                                    </>:
                                                    <Card.Title>{q.price} KGS</Card.Title>}
                                            </div>
                                            { serch ?
                                                <div className={'knopka'} style={{width:'100%'}}>
                                                    <Button onClick={() => Context.subtotal(serch)} variant={"danger"}>-</Button>
                                                    <h4>{serch.total}</h4>
                                                    <Button onClick={() => Context.inctotal(serch)} variant={"success"}>+</Button>
                                                </div>:
                                                <Button variant="primary" onClick={()=> add(q)} style={{margin: '2vh'}}>В корзину</Button>}
                                        </div>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row>
                </Container>
            </div>
        </>
    )
}

export default Product