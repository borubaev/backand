import {MyContext} from "./App";
import {useContext} from "react";
import data from './products'
import {Card, Button} from "react-bootstrap";
import numberSeparator from 'number-separator'



function Korzina() {
    const Context = useContext(MyContext)
    const dat = data.filter((e)=>{
        return Context.basket.find((s)=> {
            return s.id === e.id
        })
    })
    let z = 0

    dat.map(w => {
        const a = Context.basket.find((e)=> {
            return w.id === e.id
        })
        z += a.total * (w.discount ? (w.price -( w.price / 100 * w.discount)):w.price)
    })
    return(
        <>
        <div style={{width: '80%',margin:'15vh auto 2vh auto', display:'flex', justifyContent:'space-between'}}>
            <h1>Корзина</h1>
            <Button variant={"primary"}>Оформить заказ ({numberSeparator(z.toFixed(2))})</Button>
        </div>
            {dat.map(q => {
                const z = Context.basket.find((e) => {
                    return e.id === q.id
                })
                return(
                    <>
                        <div style={{display:'flex',justifyContent:'center',margin:'2vh'}}>
                            <div className={'tovar'}>
                                <div style={{width:'15vw',height:'40vh'}}>
                                    <div className={'img'} style={{backgroundImage:`url(${q.image})`}} />
                                </div>
                                <div style={{flexGrow:'1',padding:'15px',flexDirection:'column',display:'flex',alignItems:'start',justifyContent:'space-around'}}>
                                    <div>
                                        {q.discount ? <Card.Title>Цена: {numberSeparator((q.price-(q.price / 100 * q.discount)).toFixed(2) + 'KGS')} <s style={{color:'red'}}>{q.price} KGS</s></Card.Title>:
                                        <Card.Title>Цена: {q.price}KGS</Card.Title>}
                                        {q.discount ?  <div className={'discount d2'}>-{q.discount}%</div>:
                                        null}
                                    </div>
                                    <Card.Title>{q.title} / {q.good}</Card.Title>
                                    <Card.Title>Сумма: {numberSeparator(q.discount ? ((q.price -(q.price / 100 * q.discount)) * z.total).toFixed(2): (q.price * z.total).toFixed(2))}</Card.Title>
                                    <div className={'knopka'} style={{width:'20%'}}>
                                        <Button onClick={()=> Context.subtotal(z)} variant={"danger"}>-</Button>
                                        <h4>{z.total}</h4>
                                        <Button onClick={()=> Context.inctotal(z)} variant={"success"}>+</Button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </>
                )
            })}
        </>
    )
}
export default Korzina