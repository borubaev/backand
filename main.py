import os


from flask_cors import CORS
from flask import Flask, request, g, jsonify

from models.document import Document
from utills.database import SessionLocal
from models.admin import Admin

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})


@app.before_request
def before_request():
    g.db = SessionLocal()


@app.after_request
def auto_request(a):
    g.db.close()
    return a


@app.route('/', methods=['GET'])
def index_get():
    return jsonify([i.to_dict() for i in g.db.query(Admin).all()])


@app.route('/login', methods=['POST'])
def login_post():
    login = request.json['login']
    password = request.json['password']

    user = g.db.query(Admin)\
        .filter(Admin.login == login)\
        .filter(Admin.password == password).first()

    if user:
        return user.to_dict()
    else:
        return {}, 401


@app.route('/upload', methods=['POST'])
def upload_post():
    file = request.files['file']
    file.save(os.path.join(os.getcwd(), 'uploads', file.filename))
    item = Document()
    item.name = file.filename
    item.path = '/uploads/' + file.filename
    g.db.add(item)
    g.db.commit()
    return jsonify(item.to_dict())


@app.route('/document', methods=['GET'])
def document_get():
    _id = request.args.get('id')
    if _id is not None:
        return jsonify(
            g.db.query(Document)
                .filter(Document.id == _id)
                .one()
                .to_dict()
        )
    return jsonify([i.to_dict() for i in g.db.query(Document).all()])


@app.route('/delete', methods=['DELETE'])
def delete_post():
    _id = int(request.args['id'])
    item = g.db.query(Document).filter(Document.id == _id).one()
    os.remove(os.path.join(os.getcwd(), 'uploads', item.name))
    g.db.delete(item)
    g.db.commit()
    return 'Succesful deleted'


app.run(debug=True, host='0.0.0.0')
