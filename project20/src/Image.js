import axios from "axios";
import {useState} from "react";
import Comp from "./Comp";

function Image() {
    const [value, setValue] = useState('')
    const [mas, setMas] = useState([])
    const add = ()=> {
        const a = axios.get(`https://imsea.herokuapp.com/api/1?q=${value}`)
        a.then(q=> {
            setMas(q.data.results)
        })
    }
    console.log(mas)
    return(
        <>
           <div style={{display:'flex',justifyContent:'center', alignItems:'center', flexDirection:'column'}}>
               <h1>Search Images: {value}</h1>
               <div>
                   <input type="text" onChange={(event)=> setValue(event.target.value)} />
                   <button onClick={add}>Search</button>
               </div>
           </div>
            {mas.map(q=>  <Comp name={mas} val={value}/>)}
        </>
    )

}
export default Image










