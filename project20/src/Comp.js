import './App.css'
function Comp(props) {
    return(
        <>
            <div className={'box'}>
                <img className={'img'} src={props.name}/>
                <h1>{props.val}</h1>
            </div>
        </>
    )

}
export default Comp