import './App.css';
import {useState} from "react";
import axios from "axios";

function App() {
    const [kurs, setKurs] = useState(null)
    const [num, setNum] = useState('')
    const a = axios.get(`https://open.er-api.com/v6/latest/USD`)

    const obmen = ()=> {
        a.then(q => {
            setKurs(q.data.rates.KGS * num)
        })
    }

  return (
   <>
       <h1>{kurs === undefined || kurs === null ? 0: kurs}</h1>
        <input type="text" value={num} onChange={(event) => setNum(event.target.value)}/>
        <button onClick={obmen}>Convert</button>
   </>
  );
}

export default App;
