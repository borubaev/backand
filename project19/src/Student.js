import './App.css'
import {useEffect, useState} from 'react'
import {Button} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Navbar, Form, Nav, ListGroup} from 'react-bootstrap'

function Student() {
    const [count, setCount] = useState(JSON.parse(localStorage.getItem('os')) || [])

    const add = () => {
        const stu = prompt('Student name?')
        const ball = +prompt('Students score?')
        setCount(q => [...q, {name: stu, score: ball}])
    }
    useEffect(() => {
        localStorage.setItem('os', JSON.stringify(count))
    }, [count])
    const score = (i) => {
        const p = +prompt('Add score')
        setCount(q => {
            const a = [...q]
            const z = p > 5 ? 5 : p
            a[i].score += z  
            return a
        })
    }
    const delAll = () => {
        localStorage.setItem('os', '[]')
        setCount([])
    }
    return (
        <>
            <Navbar bg="dark" variant="dark" style={{height: '120px'}}>
                <Container fluid style={{display: 'flex', justifyContent: 'space-between'}}>
                    <Navbar.Brand href="#home" style={{fontSize: '50px'}}>
                        <img
                            alt=""
                            src="https://lh4.googleusercontent.com/XHxewbpGnE_Z-15z217NbvazviJIexZw-LUCk5qiw8zS-ZwK4QR2L5LXWd5yeOEs6334vq4I78aqp8IrWf7w-NE6RC1sdWfxW-6kAX59St-TS8-wDcj9NqIVw4AyOGy3z2dNo5cf"
                            width="80"
                            height="80"
                            className="d-inline-block align-top"
                        />{' '}
                        Students ({count.length})
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll"/>
                    <Form className="d-flex">
                        <Button variant="outline-light" style={{width: '150px', height: '50px', fontSize: '20px'}}
                                onClick={add}>Add Students</Button>
                    </Form>
                </Container>
            </Navbar>
            <div style={{boxShadow:'5px 5px 13px blue',width:'60vw', margin:'auto'}}>
                <div className="spisok s" style={{marginTop: '50px'}}>
                    <div className={'id'}>Id</div>
                    <div className={'id'}>Student</div>
                    <div className={'id'}>Score</div>
                    <div className={'act'}>Action</div>
                    {/*<Button className={"act"} variant="outline-secondary">Add score</Button>*/}
                </div>
                {count.map((q, index) => {
                    return (
                        <div className={'spisok c'}>
                            <div className={'id'}>{index + 1}</div>
                            <div className={'id'}>{q.name}</div>
                            <div className={'id'}>{q.score}</div>
                            <Button variant="success" className={'act'} onClick={() => score(index)}>Add Score</Button>
                        </div>
                    )
                })}
            </div>
            <Button variant="outline-danger" className={'clear'} onClick={delAll}>Clear All</Button>
        </>
    )
}

export default Student