import {useEffect, useState} from 'react'
import axios from "axios";
import {Link, useParams} from "react-router-dom";
import {Card, Col, ListGroup, Nav, Row} from "react-bootstrap";
import './App.css'
import NavBar from "./NavBar";


function Genre() {
    const {id} = useParams()
    const [cinema,setCinema] = useState([])
    const [genre, setGenre] = useState([])
    useEffect(()=>{
       const a = axios.get(`https://api.themoviedb.org/3/discover/movie?with_genres=${id}&api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`).then(q=> {
           console.log(q.data)
       })
        const z = axios.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
            z.then(q=>{
                setGenre(q.data.genres)
            })
    },[cinema])
    return(
        <>
            <NavBar/>
            <div className={'kan'}>
                <Row md={4} className="g-4">
                    {cinema.map(q=>{
                        return(
                            <Col>
                                <Card className={'box'}>
                                    <Card.Img className={'sur'} variant="top" src={`https://image.tmdb.org/t/p/w500${q.poster_path}`} />
                                    <Card.Body>
                                        <Card.Title>{q.title}</Card.Title>
                                    </Card.Body>
                                </Card>
                            </Col>
                        )
                    })}
                </Row>
                <div className='categories'>
                    <Nav>
                        <ListGroup variant={'flush'} style={{
                            borderRadius: '5px',
                            boxShadow: '5px 5px 9px grey',
                            border: '1px solid grey'
                        }}>
                            {genre.map(q => {
                                return (
                                    <Nav.Link as={Link} to={`/c/${q.id}`}>
                                        <ListGroup.Item style={{fontSize: '18px'}}>{q.name}</ListGroup.Item>
                                    </Nav.Link>
                                )
                            })}
                        </ListGroup>
                    </Nav>
                </div>
            </div>
        </>
    )
}
export default Genre