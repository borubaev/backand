import {Card, Col} from "react-bootstrap";
import './App.css'

function Movie(props) {
    return(
        <>
            <Col>
                <Card className={'box'}>
                    <Card.Img className={'sur'} variant="top" src={`https://image.tmdb.org/t/p/w500${props.name.name.poster_path}`} />
                    <Card.Body>
                        <Card.Title>{props.name.title === 'popular' ? props.name.name.title || props.name.name.name:props.name.name.title}</Card.Title>
                    </Card.Body>
                </Card>
            </Col>
        </>
    )
}
export default Movie