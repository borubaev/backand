import {Row} from "react-bootstrap";
import Movie from "./Movie";
import './App.css'
import {useEffect, useState} from "react";
import axios from "axios";

function Tv_or_Cinema(props) {
    const [sos, setSos] = useState(false)
    const [cinema, setCinema] = useState([])
    const [tv, setTv] = useState([])
    useEffect(() => {
        const z = axios.get(`https://api.themoviedb.org/3/${props.name.type === 'popular' ? 'movie/popular' : `trending/movie/${sos ? 'day' : 'week'}`}?api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
        z.then(val => {
                setCinema(val.data.results)
            }
        )
        const q = axios.get(`https://api.themoviedb.org/3/${props.name.type === 'popular' ? 'tv/popular' : `trending//movie/${sos ? 'day' : 'week'}`}?api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
        q.then(q => {
            setTv(q.data.results)
        })
    }, [sos])
    return (
        <>
            <div className='movies'>
                <div className={'trend'}>
                    <h3 style={{fontSize: '40px'}}>{props.name.type === 'popular' ? 'Что популярно' : 'В тренде'}</h3>
                    <div className={'btn'}>
                        <button active={!sos} onClick={() => setSos(true)}
                                className={`btn1 ${sos ? 'btn2' : null}`}>{props.name.type === 'popular' ? 'По TV' : 'Сегодня'}
                        </button>
                        <button active={sos} onClick={() => setSos(false)}
                                className={`btn1 ${!sos ? 'btn2' : null}`}>{props.name.type === 'popular' ? 'В кинотеатрах' : 'В этой неделе'}
                        </button>
                    </div>
                </div>
                <Row md={4} className="g-4">
                    {((sos ? tv : cinema).slice(0, 8)).map(q => <Movie name={{name: q, title: props.name.type}}/>)}
                </Row>

            </div>
        </>
    )
}

export default Tv_or_Cinema