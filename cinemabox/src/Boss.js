import {useEffect, useState} from "react";
import axios from "axios";
import NavBar from "./NavBar";
import {ListGroup, Nav, Spinner} from "react-bootstrap";
import Tv_or_Cinema from "./Tv_or_Cinema";
import {Link} from "react-router-dom";

function Boss() {
    const [genre, setGenre] = useState([])
    const [spin, setSpin] = useState(false)
    useEffect(() => {
        const a = axios.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
        a.then(q => {
            setGenre(q.data.genres)
        })
    }, [genre])
    setTimeout(() => {
        setSpin(true)
    }, 2000)
    return (
        <>
            <NavBar/>
            <div className="kan">
                {!spin ?
                    <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div> :
                    <>
                        <div>
                            <Tv_or_Cinema name={{type: 'popular'}}/>
                            <Tv_or_Cinema name={{type: 'trend'}}/>
                        </div>
                        <div className='categories'>
                            <Nav>
                                <ListGroup variant={'flush'} style={{
                                    borderRadius: '5px',
                                    boxShadow: '5px 5px 9px grey',
                                    border: '1px solid grey'
                                }}>
                                    {genre.map(q => {
                                        return (
                                            <Nav.Link as={Link} to={`/c/${q.id}`}>
                                                <ListGroup.Item style={{fontSize: '18px'}}>{q.name}</ListGroup.Item>
                                            </Nav.Link>
                                        )
                                    })}
                                </ListGroup>
                            </Nav>
                        </div>
                    </>}
            </div>
        </>
    )
}

export default Boss