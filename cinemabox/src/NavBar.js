import {Navbar, Container, Nav, NavDropdown} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
import {useEffect, useState} from "react";
import axios from "axios";
import {Link} from "react-router-dom";
function NavBar(props) {
    const [genremov, setGenremov] = useState([])
   useEffect(()=>{
       const a = axios.get(`https://api.themoviedb.org/3/genre/tv/list?api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
       a.then(q => {
           setGenremov(q.data.genres)
       })
   },[])
    return(
        <>
            <Navbar bg="dark" variant="dark">
                <Container fluid>
                    <Navbar.Brand href="#home">
                        <div style={{
                            display: 'flex',
                            backgroundColor: 'white',
                            alignItems: 'center',
                            borderRadius: '30px'
                        }}>
                            <div className={'content'}/>
                            <div className={'img'}/>
                        </div>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="navbarScroll"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="#home" as={Link} to={'/'}>Главная</Nav.Link>
                            <Nav.Link href="#link" as={Link} to={'/movie'}>Фильмы</Nav.Link>
                            <Nav.Link href="#link" as={Link} to={'/series'}>Сериалы</Nav.Link>
                            <Nav.Link href="#link" as={Link} to={'/person'}>Люди</Nav.Link>
                            <NavDropdown title="Жанры TV" id="basic-nav-dropdown">
                                {genremov.map((q, i) => {
                                    return (
                                        <NavDropdown.Item href="#action/3.1">{q.name}</NavDropdown.Item>
                                    )
                                })}
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>

                </Container>
            </Navbar>
        </>
    )

}
export default NavBar