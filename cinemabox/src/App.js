import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'
import Boss from "./Boss";
import {BrowserRouter,Routes,Route} from "react-router-dom";
import Tv_or_Cinema from "./Tv_or_Cinema";
import Genre from "./Genre";

function App() {
    return(
        <>
            <BrowserRouter>
                <Routes>
                    <Route path={'/'} element={<Boss/>}/>
                    <Route path={'/movie'} element={<Tv_or_Cinema/>}/>
                    <Route path={'/c:id'} element={<Genre/>}/>
                </Routes>
            </BrowserRouter>
        </>
    )
}

export default App