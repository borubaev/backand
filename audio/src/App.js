import  {useRef} from 'react'
function App() {
  const audio = useRef()
  return(
      <>
        <audio ref={audio} controls>
          <source src={`https://chaqqon.net/uploads/files/2022-07/mirbek-atabekov-kechki-bishkek_(chaqqon.net).mp3`} type="audio/mpeg"/>
        </audio>
          <button onClick={()=>{
              audio.current.play()
              // audio.current.pause()
              // audio.current.currentTime = 0
          }}>Play</button>
      </>
  )
}
export default App