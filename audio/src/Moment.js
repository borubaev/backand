import moment from "moment";
import {useState} from "react";
import 'moment/locale/ky'

moment.locale('ky')
function Moment() {
    const [clock,setClock] = useState('')
    setInterval(()=>{
        setClock(q => moment().format('MMMM Do YYYY, h:mm:ss a'))
    }, 1000)
    return(
        <>
            <div>
                {clock}
            </div>
        </>
    )
}
export default Moment