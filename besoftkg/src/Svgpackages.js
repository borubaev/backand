function Svgpackages(props) {

    return (
        <>
            <div className={'svg'}>
                {props.type === '1' ?
                    <svg width="88" height="88" xmlns="http://www.w3.org/2000/svg">
                        <g fill="none" fill-rule="nonzero">
                            <path
                                d="M54 56h-9a2 2 0 0 1-2-2V43a2 2 0 0 1 2-2h9a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2zm-9-13v10h9V43h-9z"
                                fill="#FCAC96"/>
                            <path d="M41 50h-7V34h14v5h2v-5a2 2 0 0 0-2-2H34a2 2 0 0 0-2 2v18a2 2 0 0 0 2 2h7v-4z"
                                  fill="#FC8464"/>
                        </g>
                    </svg> :
                        <svg width="88" height="88" xmlns="http://www.w3.org/2000/svg">
                            <g fill="none" fill-rule="nonzero">
                                <path d="M33 47v7a13 13 0 0 0 13-13v-7c-7.18 0-13 5.82-13 13z" fill="#FF6381"/>
                                <path d="M22 41v4a9 9 0 0 0 9 9v-4a9 9 0 0 0-9-9z" fill="#FF97AA"/>
                            </g>
                        </svg>}
            </div>
        </>
    )
}

export default Svgpackages