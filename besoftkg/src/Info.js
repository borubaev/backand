import {Row,Col,Card,Button} from "react-bootstrap";

function Info() {
    const jobs = [
        {
            img: `https://www.luxoft.com/upload/medialibrary/3ea/how_to_deliver.png`,
            title: `BANKING AND CAPITAL MARKETS`,
            opis: `How to deliver your data strategy`,
            overview: `The financial services industry has always relied on data and accurate record-keeping.`
        },
        {
            img: `https://www.luxoft.com/upload/medialibrary/657/423188_AU_Top_Company_Guide.png`,
            title: `AUTOMOTIVE`,
            opis: `Face to face: An interview with Luz G. Mauch`,
            overview: `Our Executive VP of Automotive predicts that the future of the automotive industry will require hardware and 
            software development to go their separate ways.
             Learn about Luxoft’s role in the car software revolution in this interview for Top Company Guide.`
        },
        {
            img: `https://www.luxoft.com/upload/medialibrary/563/behavioral_archetypes.png`,
            title: `STRATEGY AND DESIGN THINKING`,
            opis: `Behavioral archetypes toolkit for assessing customer persona`,
            overview: `Now more than ever, a company lives or dies on the quality and credibility of the experiences they provide for their customers.`
        },
    ]

    return (
        <>
            <div className={'boss'}>
                <h1 style={{margin:'10vh 0',color:'rgba(180,19,139,0.79)'}}>О нашей компании</h1>
                <div>
                    <Row md={12} style={{justifyContent:'center'}}>
                        {jobs.map(q=>{
                            return(
                                <Col md={3}>
                                    <Card className={'cardi'}>
                                        <Card.Img variant="top" src={q.img} className={'cardimg'}/>
                                        <Card.Body>
                                            <Card.Title>{q.title}</Card.Title>
                                            <Card.Title>{q.opis}</Card.Title>
                                            <Card.Text>{q.overview}</Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row>
                </div>
                <Button variant="outline-success" style={{margin:'3vh 0'}}>Посмотреть больше</Button>

            </div>
        </>
    )
}

export default Info