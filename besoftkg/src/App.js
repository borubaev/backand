import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./Header";
import Nbar from "./Nbar";
import Info from "./Info";
import Uslugi from "./Uslugi";
import Personal from "./Personal";

function App() {
  return (
      <>
          <Nbar/>
          <Header/>
          <Info/>
          <Uslugi/>
          <Personal/>
      </>

  );
}

export default App;