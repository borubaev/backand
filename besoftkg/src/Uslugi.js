import Svgpackages from "./Svgpackages";

function Uslugi() {

    return (
        <>
            <div className={'boss'} style={{backgroundColor:`rgba(206,212,205,0.33)`}}>
                <h1 style={{margin: '7vh 0', color: 'rgba(50,150,229,0.79)'}}>Наши услуги</h1>
                <div className={'svgboss'}>
                    <div style={{width: '35%'}}>
                        <div className={'card'}>
                            <Svgpackages type={'2'}/>
                            <p className={'text'}>Мобильное приложение</p>
                            <span> Вы можете заказать разработку, будь то для продажи, игры, для компании, или же описать нам
                               основную концепцию проекта. С программой лояльности, интернет-магазином, CRM-системой или
                               облачной ATC.</span>
                        </div>
                    </div>
                    <div style={{width: '35%', marginTop: '17vh'}}>
                        <div className={'card'}>
                            <Svgpackages type={'1'}/>
                            <p className={'text'}>Веб-сайт</p>
                            <span>Разрабатываем индивидуальные web-сервисы корпоративные сайты и порталы.</span>
                        </div>
                    </div>
                    <div style={{width: '35%', margin: 'vh 0'}}>
                        <div className={'card'}>
                            <Svgpackages type={'1'}/>
                            <p className={'text'}>UI/UX дизайн
                            </p>
                            <span>Веб-разработка, в задачи которой входит проектирование пользовательских веб-интерфейсов для сайтов или веб-приложении</span>
                        </div>
                    </div>
                    <div style={{width: '35%', marginTop: '22vh'}}>
                        <div className={'card'}>
                            <Svgpackages type={'1'}/>
                            <p className={'text'}>SEO оптимизация
                            </p>
                            <span>Продвигаем сайты в поисковых системах с высоким показателем ROMI.</span>
                        </div>
                    </div>


                </div>
            </div>
        </>
    )
}

export default Uslugi