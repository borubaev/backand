import 'bootstrap/dist/css/bootstrap.min.css';
import {Carousel} from 'react-bootstrap'


function Header() {

    return (
        <>
            <Carousel variant="dark" style={{margin:'0',padding:'0'}}>
                <Carousel.Item>
                    <div style={{width: '100vw'}}>
                        <div className={'img'} style={{backgroundImage: `url(https://wallpaperaccess.com/full/186244.jpg)`}}/>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div style={{width: '100vw'}}>
                        <div className={'img'} style={{backgroundImage: `url(https://www.fintechfutures.com/files/2021/08/AdobeStock_267083342.jpeg)`}}/>
                    </div>
                </Carousel.Item>
                <Carousel.Item>
                    <div style={{width: '100vw'}}>
                        <div className={'img'} style={{backgroundImage: `url(https://res.cloudinary.com/practicaldev/image/fetch/s--n9tJX35q--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://dev-to-uploads.s3.amazonaws.com/uploads/articles/dffmn41w40agufotxhzh.jpg)`}}/>
                    </div>
                </Carousel.Item>
            </Carousel>
        </>
    )
}

export default Header