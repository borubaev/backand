import Container from "react-bootstrap/Container";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar,Nav,Form} from "react-bootstrap";
import {useState} from "react";


function Nbar() {
    const[tr,setTr] = useState(true)

    return(
        <>
            <Navbar bg="light" variant="light" expand="lg" style={{position:'fixed',zIndex:'5',width:'100vw',padding:'2.5vh 0'}}>
                <Container style={{maxWidth: '90vw'}}>
                    <Navbar.Brand href="#home">
                        <h1>Besoft Group</h1>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <div style={{display: 'flex'}}>
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="me-auto" style={{alignItems:'center'}}>
                                <Nav.Link href="#home">Главная</Nav.Link>
                                <Nav.Link href="#link">О нас</Nav.Link>
                                <Nav.Link href="#link">Наши услуги</Nav.Link>
                                <Nav.Link href="#link">IT Академия</Nav.Link>
                                {!tr ? <Form className="d-flex">
                                    <Form.Control
                                        type="search"
                                        placeholder="Search"
                                        className="me-2"
                                        aria-label="Search"
                                    />
                                </Form>:null}
                                <Nav.Link href="#link" onClick={()=> setTr(q => !q)}>
                                    <i className="fa fa-search fa-2x"></i>
                                </Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </div>
                </Container>
            </Navbar>
        </>
    )
}
export default Nbar