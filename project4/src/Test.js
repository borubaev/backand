import {useState} from 'react'
import './App.css'

function Test() {
    const [san, setSan] = useState(0)

    const changeName = () => {
        setSan( san + 1)
    }
    if (san > 10) {
        setSan(0)
    }

    return (
        <>
           <div className='poza'>
               <div>{san}
                 <button onClick={changeName}>+</button>
               </div>
           </div>
        </>
    )
}
    export default Test