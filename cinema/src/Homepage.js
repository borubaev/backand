import {useState} from "react";
import {Spinner} from "react-bootstrap";
import Tv_or_Cinema from "./Tv_or_Cinema";

function Homepage(props) {
    const [load,setLoad]= useState(true)
    setTimeout(()=>{
        setLoad(false)
    }, 1000)

    return (
        <>
            {load ?
                    <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div> :
                <>
                    <div>
                        <Tv_or_Cinema name={{type: 'popular', set:props.name}}/>
                        <Tv_or_Cinema name={{type: 'trend'}}/>
                    </div>
                </>}
        </>
    )
}

export default Homepage