import {useState, useEffect} from 'react'
import {Nav,ListGroup} from "react-bootstrap";
import axios from "axios";
import {Link, useParams} from "react-router-dom";
import './App.css'

function Genre(props) {
    const [genre, setGenre] = useState([])
    useEffect(()=>{
        const z = axios.get(`https://api.themoviedb.org/3/genre/movie/list?api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
        z.then(q=>{
            setGenre(q.data.genres)
        })
    },[])
    return (
        <>
            <Nav>
                <ListGroup variant={'flush'} style={{
                    borderRadius: '5px',
                    boxShadow: '5px 5px 9px grey',
                    borderBottom: '1px solid grey',
                    width:'23vw',
                    marginTop:'4vw'
                }}>
                    {genre.map(q => {
                        return (
                            <Nav.Link as={Link} to={`/c/${q.id}`}>
                                <ListGroup.Item style={{fontSize: '18px',border:'none', borderBottom:'1px solid grey', padding:'0'}} onClick={()=> props.name(false)}>{q.name}</ListGroup.Item>
                            </Nav.Link>
                        )
                    })}
                </ListGroup>
            </Nav>
        </>
    )

}
export default Genre