import './App.css'
import {useEffect, useState} from "react";
import axios from "axios";
import {Card, Col, Spinner} from "react-bootstrap";
import {Link, useParams} from "react-router-dom";


function MovPerSer() {
    const {par} = useParams()
    const [movie, setMovie] = useState([])
    const [spin, setSpin] = useState(true)
    useEffect(() => {
        setTimeout(() => {
            setSpin(false)
        }, 700)
    }, [movie])
    useEffect(() => {
        const a = axios.get(`https://api.themoviedb.org/3/${par === 'movie' ? 'movie/top_rated' : par === 'series' ? 'tv/popular' : par === 'person' ? 'person/popular' : ''}?api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
        a.then(q => {
            setMovie(q.data.results)
            setSpin(true)
        })
    }, [par])
    return (
        <>
            {spin ? <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </div> :
                <div className={'movies'}>
                    <div className={'trend'}>
                        <h1>{par === 'movie' ? 'Фильмы' : par === 'series' ? 'Сериалы' : 'Люди'}</h1>
                    </div>
                    {movie.map(q => {
                        return (
                            <>
                                <Col>
                                    <Card className={'box'} as={Link} to={`/s/a/${q.id}`}>
                                        <Card.Img className={'sur'} variant="top"
                                                  src={`https://image.tmdb.org/t/p/w500${q.poster_path || q.profile_path}`}/>
                                        <Card.Body>
                                            <Card.Title>{q.title || q.name}</Card.Title>
                                        </Card.Body>
                                    </Card>
                                </Col>

                            </>
                        )
                    })}
                </div>}
        </>
    )
}

export default MovPerSer