import {BrowserRouter, Routes, Route, Link} from "react-router-dom";
import axios from "axios";
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar, Container, Col, Nav, NavDropdown, Row} from "react-bootstrap";
import {useState, useEffect} from 'react'
import Genre from "./Genre";
import Homepage from "./Homepage";
import GenreKat from "./GenreKat";
import MovPerSer from "./MovPerSer";
import Info from "./Info";
import Mov from "./Mov";

function App() {
    const [genremov, setGenremov] = useState([])
    const [spin, setSpin] = useState(false)
    const [val, setVal] = useState(false)
    const [num, setNum] = useState(1)
    const [movtv,setMovtv] = useState(false)
    setTimeout(() => {
        setSpin(true)
    }, 2000)

    console.log(movtv)
    useEffect(() => {
        const a = axios.get(`https://api.themoviedb.org/3/genre/tv/list?api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
        a.then(q => {
            setGenremov(q.data.genres)
        })
    }, [])
    return (
        <>
            <BrowserRouter>
                <Navbar bg="dark" variant="dark">
                    <Container fluid>
                        <Navbar.Brand href="#home">
                            <img
                                src="https://img.freepik.com/premium-vector/cinema-camera-roll-film-logo-design-template_527727-210.jpg"
                                width="80px"
                                height="80px"
                                className="d-inline-block align-top"
                                style={{borderRadius: '50%'}}
                            />
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="navbarScroll"/>
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="me-auto">
                                <Nav.Link href="#link" as={Link} to={'/'}onClick={() => setNum(1)}>Главная</Nav.Link>
                                <Nav.Link href="#link" as={Link} to={'/s/movie'}
                                          onClick={() => setNum(1)}>Фильмы</Nav.Link>
                                <Nav.Link href="#link" as={Link} to={'/s/series'}
                                          onClick={() => setNum(2)}>Сериалы</Nav.Link>
                                <Nav.Link href="#link" as={Link} to={'/s/person'}
                                          onClick={() => setNum(3)}>Люди</Nav.Link>
                                <NavDropdown title="Жанры TV" id="basic-nav-dropdown">
                                    {genremov.map((p, i) => {
                                        return (
                                            <NavDropdown.Item href="#action/3.1" as={Link} to={`/c/${p.id}`}
                                                              onClick={() => setVal(true)}>{p.name}</NavDropdown.Item>
                                        )
                                    })}
                                </NavDropdown>
                            </Nav>
                        </Navbar.Collapse>

                    </Container>
                </Navbar>
                <Row md={12} className="g-4">
                    <Col sm={9} style={{display: 'flex', marginLeft: '25px'}}>
                        <Routes>
                            <Route path={'/c/:id'} element={<GenreKat name={val} spin={spin}/>}/>
                            <Route path={'/'} element={<Homepage name={setMovtv}/>}/>
                            <Route path={'/s/:par'} element={<MovPerSer spin={spin}/>}/>
                            <Route path={`/q/:card`} element={<Info name={val}/>}/>
                            <Route path={`/s/a/:tof`} element={<Mov num={num} name={movtv}/>}/>
                        </Routes>
                    </Col>
                    <Col sm={2}>
                        <Genre name={setVal}/>
                    </Col>
                </Row>
            </BrowserRouter>
        </>

    )
}

export default App