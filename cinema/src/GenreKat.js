import {Card, Col, Spinner} from "react-bootstrap";
import {useState, useEffect} from 'react'
import {Link, useParams} from 'react-router-dom'
import axios from "axios";
import './App.css'

function GenreKat(props) {
    const {id} = useParams()
    const [loadin, setLoadin] = useState(true)
    const [movie, setMovie] = useState([])
    useEffect(() => {
        setTimeout(() => {
            setLoadin(false)
        }, 500)
    }, [movie])
    useEffect(() => {
        const a = axios.get(`https://api.themoviedb.org/3/discover/${props.name === true ? 'movie' : 'tv'}
        ?with_genres=${id}&api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
        a.then(q => {
            setMovie(q.data.results)
            setLoadin(true)
        })
    }, [id])
    return (
        <>
            {loadin ?
                <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </div> :
                <>
                    <div className={'movies'}>
                        {movie.map(q => {
                            return (
                                <>
                                    <Col>
                                        <Card className={'box'} as={Link} to={`/q/${q.id}`}>
                                            <Card.Img className={'sur'} variant="top"
                                                      src={`https://image.tmdb.org/t/p/w500${q.poster_path}`}/>
                                            <Card.Body>
                                                <Card.Title>{q.title || q.name}</Card.Title>
                                            </Card.Body>
                                        </Card>
                                    </Col>

                                </>
                            )
                        })}
                    </div>
                </>}
        </>
    )
}

export default GenreKat