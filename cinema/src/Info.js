import {Row, Col, Card, Spinner} from "react-bootstrap";
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Link, useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import axios from "axios";


function Info(props) {
    const {card} = useParams()
    const [opis, setOpis] = useState({})
    const [time, setTime] = useState(true)
    useEffect(()=>{
        setTimeout(()=>{
            setTime(false)
        },800)
    },[opis])
    useEffect(() => {
        if (props.name === true) {
            const a = axios.get(`https://api.themoviedb.org/3/movie/${card}?api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
            a.then(q => {
                setOpis(q.data)
                setTime(true)
            })
        }else {
            const a = axios.get(`https://api.themoviedb.org/3/tv/${card}?api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
            a.then(q => {
                setOpis(q.data)
            })
        }
        setTime(true)
    }, [props.name])
    return (
        <>
            {time ?
                <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </div>:
                <>
                    <Row xs={1} md={2} className="g-4">
                        <Col>
                            <Card className={'bgi'}>
                                <div style={{position: 'relative'}}>
                                    <Card.Img variant="top" className={'img'}
                                              src={`https://image.tmdb.org/t/p/original${opis.backdrop_path}`}/>
                                    <div className={'contimg'}>
                                        <img src={`https://image.tmdb.org/t/p/original${opis.poster_path}`}
                                             width={'100%'}/>
                                    </div>
                                    <div className={'conttext h1'}>
                                        <Card.Title><h1>{opis.title || opis.name}</h1></Card.Title>
                                    </div>
                                    <div className={'conttext h2'}>
                                        <h2>{opis.genres ? opis.genres.map(q => {
                                            return <span>{q.name}, </span>
                                        }):null}</h2>
                                    </div>
                                </div>
                                <Card.Body style={{marginTop: '10vh'}}>
                                    <Card.Text>
                                        {opis.overview}
                                    </Card.Text>

                                </Card.Body>

                            </Card>
                        </Col>
                    </Row>
                </>
            }

        </>
    )
}

export default Info