import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import axios from "axios";
import {Card, Col, Row, Spinner} from "react-bootstrap";
import './App.css'

function Mov(props) {
    const {tof} = useParams()
    const [clas, setClas] = useState({})
    const [spintime,setSpintime] = useState(true)
    useEffect(()=>{
        setTimeout(()=>{
            setSpintime(false)
        },700)
    },[clas])
    useEffect(() => {
        const a = axios.get(`https://api.themoviedb.org/3/${props.name ? 'tv/'+tof: 'movie/'+tof}?api_key=1ea54fff90d26ae7dc1f5e21fe637664&language=ru`)
            a.then(q => {
            setClas(q.data)
            setSpintime(true)
        })
    }, [tof])
    return (
        <>
            {spintime ? <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                <Spinner animation="border" role="status">
                    <span className="visually-hidden">Loading...</span>
                </Spinner>
            </div>:
                <Row xs={1} md={2} className="g-4">
                    <Col>
                        <Card className={props.num === 3 ? 'prof':'bgi'}>
                            <div style={{position: 'relative'}}>
                                <Card.Img variant="top" className={'img'}
                                          src={props.num !== 3 ? `https://image.tmdb.org/t/p/original${clas.backdrop_path}`:`https://blogerbox.com/d/1377.png`}/>
                                <div className={'contimg'}>
                                    <img src={`https://image.tmdb.org/t/p/original${clas.poster_path || clas.profile_path}`}
                                         width={'100%'}/>
                                </div>
                                <div className={'conttext h1'}>
                                    <Card.Title>
                                        <h1>{props.num === 2 ? clas.title : props.num === 1 ? clas.name : clas.name}</h1>
                                    </Card.Title>
                                </div>
                                <div className={'conttext h2'}>
                                    <h2>{clas.genres ? clas.genres.map(q => {
                                        return <span>{q.name}, </span>
                                    }) : null}</h2>
                                </div>
                            </div>
                            <Card.Body style={{marginTop: '10vh'}}>
                                <Card.Text>
                                    {props.num === 3 ? clas.biography : clas.overview}
                                </Card.Text>

                            </Card.Body>

                        </Card>
                    </Col>
                </Row>
            }

        </>
    )
}

export default Mov