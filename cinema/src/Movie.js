import {Card, Col} from "react-bootstrap";
import './App.css'
import {Link} from "react-router-dom";

function Movie(props) {
    return (
        <>
            <Col sm={3}>
                <Card className={'box'} as={Link} to={`/s/a/${props.name.name.id}`}>
                    <Card.Img className={'sur'} variant="top"
                              src={`https://image.tmdb.org/t/p/w500${props.name.name.poster_path}`}/>
                    <Card.Body>
                        <Card.Title>{props.name.title === 'popular' ? props.name.name.title || props.name.name.name : props.name.name.title}</Card.Title>
                    </Card.Body>
                </Card>
            </Col>
        </>
    )
}

export default Movie