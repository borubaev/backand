import 'bootstrap/dist/css/bootstrap.min.css';
import logo from './logo.svg'
import {Navbar, Container, Form, Button, Row, Col, Card,Spinner} from "react-bootstrap";
import './App.css';
import {useEffect, useState} from "react";
import axios from "axios";

function App() {
    const [basket, setBasket] = useState([])
    const [name, setName] = useState('')
    const [num, setNum] = useState(0)
    const [tr, setTr] = useState(true)
    const [spin, setSpin] = useState(true)
    setTimeout(() => {
        setSpin(false)
    }, 700)
    const zapros = () => {
        setTr(q => !q)
        setNum(q => q += 20)
        setSpin(true)
    }
    useEffect(() => {
        const a = axios.get(`https://api.giphy.com/v1/gifs/search?q=${name}&limit=${num}&api_key=PfFQ8roY49VWXS2NmHUxxzvEatmVtE4l`)
        a.then(q => {
            setBasket(q.data.data)
        })
    }, [tr])
    return (
        <>
            <Navbar bg="dark" variant="dark">
                <Container style={{margin: '0', maxWidth: '100vw'}}>
                    <Navbar.Brand href="#home" style={{display: 'flex', alignItems: 'center', fontSize: '50px'}}>
                        <img
                            alt=""
                            src={logo}
                            width="60"
                            height="60"
                            className="d-inline-block align-top"
                        />{' '}
                        Mems
                    </Navbar.Brand>
                    <Form className="d-flex">
                        <Form.Control
                            type="search"
                            placeholder="Search"
                            className="me-2"
                            aria-label="Search"
                            onChange={(event) => setName(event.target.value)}
                        />
                        <Button variant="outline-success" onClick={zapros}>Search</Button>
                    </Form>
                </Container>
            </Navbar>
            {basket.length === 0 && spin ? <div style={{position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </div> :
                <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column'}}>
                    <div style={{width: '100vw', height: '100%', margin: '3vh'}}>
                        <Container>
                            <Row md={12} style={{marginTop: '5vh'}}>
                                {basket.map(q => {
                                    return (
                                        <Col md={3}>
                                            <Card style={{width: '100%', marginTop: '3vh'}}>
                                                <Card.Img variant="top" src={q.images.original.url}
                                                          style={{width: "100%", height: '30vh'}}/>
                                                <Card.Body style={{width: '100%', height: '15vh'}}>
                                                    <Card.Title>{q.title}</Card.Title>
                                                </Card.Body>
                                            </Card>
                                        </Col>
                                    )
                                })}
                            </Row>
                        </Container>
                    </div>
                    {basket.length > 0 ?
                        <Button variant='outline-success' onClick={zapros}>Загрузить ещё</Button> : null
                    }
                </div>}
        </>
    );
}

export default App;
